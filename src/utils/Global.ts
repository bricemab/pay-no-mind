import axios from "axios";
import Config from "@/config/config";
export const BASE_URL = Config.backendEndPoint;

export default {
  instanceAxios: axios.create({
    baseURL: BASE_URL,
  }),
  sendRequestToSocket: (socket: any, emitString: string, data: any, callbackOnReturnSocket: any) => {
    data["token"] = sessionStorage.getItem('token');
    socket.emit(emitString, data);
    socket.on(emitString+"-emit", function(dataReturn: any) {
      callbackOnReturnSocket(dataReturn);
    });
  }
};

export const getMonthNames = () => [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre"
];
export const getWeekDays = () => [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi"
];

/*export const sendRequestToSocket = (socket: any, emitString: string, data: any, callbackOnReturnSocket: any) => {
  socket.emit(emitString, data);
  socket.on(emitString+"-emit", callbackOnReturnSocket(data));
}*/