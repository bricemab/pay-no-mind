export const Permission = {
    specialState: {
        redirectToHome: "specialState.redirectToHome",
    allowAll: "specialState.allowAll",
    userLoggedIn: "specialState.userLoggedIn",
    userLoggedOff: "specialState.userLoggedOff"
    },
    collectionsManager: {
        viewList: "collectionsManager.viewList",
        articlesManagement: "collectionsManager.articlesManagement",
    },
    apparelManager: {
        viewList: "apparelManager.viewList",
    },
    comingSoonManager: {
        viewList: "comingSoonManager.viewList",
    },
    newCollectionManager: {
        viewList: "newCollectionManager.viewList",
    },
    articlesManager: {
        articlesManagement: "articlesManager.articlesManagement",
    }
}