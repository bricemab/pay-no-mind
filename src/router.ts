import Vue from "vue";
import VueRouter from "vue-router";

import AppVue from "../App";
import DefaultLayoutCollectionVue from "@/views/layouts/CollectionsPageVue";
import CollectionsComponent from "@/components/collections/CollectionsComponent";
import NewCollectionComponent from "@/components/collections/NewCollectionComponent";
import ComingSoonComponent from "@/components/collections/ComingSoonComponent";
import ArticlesManagementComponent from "@/components/collections/ArticlesManagementComponent";
import ApparelComponent from "@/components/collections/ApparelComponent";
import NotFoundPageVue from "@/views/pages/NotFoundPageVue";
import LoginPageVue from "@/views/layouts/LoginPageVue";
import NoLayoutVue from "@/views/layouts/NoLayoutVue.vue";
import store from './store';

import {Permission} from "@/permissions";
import AclManager from "@/AclManager";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            component: Vue.component("App", AppVue),
            children: [
                {
                    path: "collections",
                    component: Vue.component("DefaultLayoutCollection", DefaultLayoutCollectionVue),
                    children: [
                        {
                            path: "collections",
                            component: Vue.component("CollectionsComponent", CollectionsComponent),
                            meta: {
                                permission: Permission.specialState.allowAll
                            },
                        },
                        {
                            path: "new-collection",
                            component: Vue.component("NewCollectionComponent", NewCollectionComponent),
                            meta: {
                                permission: Permission.specialState.allowAll
                            }
                        },
                        {
                            path: "apparel",
                            component: Vue.component("ApparelComponent", ApparelComponent),
                            meta: {
                                permission: Permission.specialState.allowAll
                            }
                        },
                        {
                            path: "coming-soon",
                            component: Vue.component("ComingSoonComponent", ComingSoonComponent),
                            meta: {
                                permission: Permission.specialState.allowAll
                            }
                        },
                        {
                            path: "articles-management",
                            component: Vue.component("ArticlesManagementComponent", ArticlesManagementComponent),
                            meta: {
                                permission: Permission.collectionsManager.articlesManagement
                            }
                        },
                        {path: "", redirect: "/collections/collections"},
                        {path: "*", redirect: "/not-found"},
                    ]
                },
                {
                    path: "user",
                    component: Vue.component("NoLayoutVue", NoLayoutVue),
                    children: [
                        {
                            path: "",
                            name: "login",
                            component: Vue.component("LoginPage", LoginPageVue),
                            meta: {
                                permission: Permission.specialState.userLoggedOff
                            }
                        },
                        {
                            path: "login",
                            name: "login",
                            component: Vue.component("LoginPage", LoginPageVue),
                            meta: {
                                permission: Permission.specialState.userLoggedOff
                            }
                        },
                        {
                            path: "not-found",
                            component: Vue.component("NotFound", NotFoundPageVue),
                            meta: {
                                permission: Permission.specialState.allowAll
                            }
                        },
                        {path: "*", redirect: "/not-found"}
                    ]
                },
                {path: "/", redirect: "/collections"}
            ]
        },
    ]
});


router.beforeEach((to, from, next) => {
    const {isAllowed, redirectionRoute} = AclManager.hasUserAccessToPermission(
        to.meta.permission
    );

    if (isAllowed) {
        next();
    } else {
        console.log(redirectionRoute);
        next(redirectionRoute);
    }
});

export default router;
